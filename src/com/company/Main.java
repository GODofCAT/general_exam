package com.company;

import java.io.*;
import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);
    static int LastId = 1;

    static class Site{

        int Id;
        String URL;
        int CountVisitedPerMonth;
        int AgeLimit;
        double SiteRating;

        Site(int Id,String URL,int CountVisitedPerMonth,int AgeLimit,double SiteRating){
            this.Id = Id;
            this.URL = URL;
            this.CountVisitedPerMonth = CountVisitedPerMonth;
            this.AgeLimit = AgeLimit;
            this.SiteRating = SiteRating;
        }

    }

    //------------SystemFunc--------------//

    static void PrintMenu(){
        System.out.println("1.add new site");
        System.out.println("2.save all sites");
        System.out.println("3.load all sites");
        System.out.println("4.sort by count visiters per mounth");
        System.out.println("5.search sites by min/max age limit");
        System.out.println("0.exit");
    }

    static void Print50Enters(){
        for (int i=0;i<50;i++){
            System.out.println();
        }
    }

    static Site[] ResizeList(Site[] oldSiteList,int newLenght){
        Site[] newSiteList = new Site[newLenght];

        int minSize = newLenght<oldSiteList.length? newLenght:oldSiteList.length;

        for (int i=0;i<minSize;i++){
            newSiteList[i] = oldSiteList[i];
        }

        return newSiteList;
    }

    static int GetMenuPoint(){
        System.out.print("input menu point: ");
        return scanner.nextInt();
    }

    //------------SystemFunc--------------//


    //------------CRUD Function--------------//

    static void PrintAll(Site[] siteList){
        System.out.printf("%-4s%-12s%-9s%-9s%-8s\n","id","url","ppl/mnth","age lim","rating");

        if (siteList!=null){
            for (int i=0;i<siteList.length;i++){
                System.out.printf("%-4d%-12s%-9d%-9d%-8f\n",siteList[i].Id,siteList[i].URL,siteList[i].CountVisitedPerMonth,siteList[i].AgeLimit,siteList[i].SiteRating);
            }
        }

        else {
            System.out.println("list is empty");
        }

        System.out.println();
    }

    static Site[] AddSite(Site[] siteList,Site site){

        if (siteList==null){
            siteList = new Site[1];
        }
        else{

            siteList = ResizeList(siteList,siteList.length+1);
        }

        siteList[siteList.length-1] = CreateSite(siteList,site,true);

        return siteList;
    }

    static Site[] SortByCountVisitersPerMounth(Site[] siteList,boolean ASC){
        int temp,offset=0;
        boolean choise,sort;

        do {
            sort = true;

            for (int i=0;i<siteList.length-1-offset;i++){

                if (ASC){
                    choise = siteList[i].CountVisitedPerMonth<siteList[i+1].CountVisitedPerMonth;
                }
                else {
                    choise = siteList[i].CountVisitedPerMonth>siteList[i+1].CountVisitedPerMonth;
                }

                if (choise){
                    sort = false;

                    temp = siteList[i+1].CountVisitedPerMonth;
                    siteList[i+1].CountVisitedPerMonth = siteList[i].CountVisitedPerMonth;
                    siteList[i].CountVisitedPerMonth = temp;
                }

                offset++;
            }
        }while (!sort);

        return siteList;
    }

    static int GetMinAge(Site[] siteList){
        int minAge=siteList[0].AgeLimit;

        for (int i=0;i<siteList.length;i++){
            if (siteList[i].AgeLimit<minAge){
                minAge = siteList[i].AgeLimit;
            }
        }
        return minAge;
    }

    static int GetMaxAge(Site[] siteList){
        int maxAge=siteList[0].AgeLimit;

        for (int i=0;i<siteList.length;i++){
            if (siteList[i].AgeLimit>maxAge){
                maxAge = siteList[i].AgeLimit;
            }
        }
        return maxAge;
    }

    static int InputMinAge(int maxPerisibleAge,int minPerisibleAge){

        System.out.print("input min age between "+minPerisibleAge+" and "+maxPerisibleAge+" : ");
        int minAge = scanner.nextInt();

        return minAge;
    }

    static int InputMaxAge(int maxPerisibleAge,int minPerisibleAge){

        System.out.print("input max age between "+minPerisibleAge+" and "+maxPerisibleAge+" : ");
        int maxAge = scanner.nextInt();

        return maxAge;
    }

    static Site[] SortByAge(Site[] siteList,int maxAge,int minAge){
        Site[] sortSiteList = null;


        for (int i=0,j=0;i<siteList.length;i++){
            if (minAge<=siteList[i].AgeLimit&&siteList[i].AgeLimit<=maxAge){

                    sortSiteList=ResizeList(siteList,j+1);
                    sortSiteList[j]=siteList[i];

                    j++;
            }
        }

        if(sortSiteList==null){
            return null;
        }

        return sortSiteList;
    }

    //------------CRUD Function--------------//




    //---------------Ext age-----------------//

    static Site CreateSite(Site[] siteList,Site site,boolean newSite){

        System.out.print("input site URL: ");
        String url = scanner.next();

        System.out.print("input count visited per month: ");
        int countVisitedPerMonth = scanner.nextInt();

        System.out.print("input age limit: ");
        int ageLimit = scanner.nextInt();

        System.out.print("input site rating: ");
        double siteRating = scanner.nextDouble();

        site = new Site(LastId,url,countVisitedPerMonth,ageLimit,siteRating);

        if (newSite){
            LastId++;
        }

        return site;
    }

    //--------------------Ext Functions--------------------//


    //--------------------File Functions--------------------//
    static void PrintAllToFile(Site[] siteList,String fileName) throws FileNotFoundException {

        PrintStream stream = new PrintStream(fileName);

        stream.printf("%-4s%-12s%-9s%-9s%-8s\n","id","url","ppl/mnth","age lim","rating");

        stream.println();

        if(siteList==null)
        {
            System.out.println("Array is Empty");
        }
        else if(siteList.length==0)
        {
            stream.println("Array is Empty");
        }
        else
        {
            for (int i = 0; i < siteList.length; i++) {
                stream.printf("%-4d%-12s%-9d%-9d%-8f\n",siteList[i].Id,siteList[i].URL,siteList[i].CountVisitedPerMonth,siteList[i].AgeLimit,siteList[i].SiteRating);
            }
        }

        stream.println();

        stream.close();
    }

    static void SaveSiteToFile(Site[] siteList,String fileName) throws FileNotFoundException {
        PrintStream stream = new PrintStream(fileName);

        stream.println(siteList.length);
        stream.println(LastId);

        for (int i=0;i<siteList.length;i++){
            stream.println(siteList[i].Id);
            stream.println(siteList[i].URL);
            stream.println(siteList[i].CountVisitedPerMonth);
            stream.println(siteList[i].AgeLimit);
            stream.println(siteList[i].SiteRating);
        }

        stream.close();
    }
    static Site[] LoadSiteFromFile(String fileName) throws IOException {
        Site[] siteList;

        int countSites;

        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        countSites = Integer.parseInt(reader.readLine());
        LastId = Integer.parseInt(reader.readLine());

        siteList = new Site[countSites];

        for (int i=0;i<siteList.length;i++){
            int id = Integer.parseInt(reader.readLine());
            String url  = reader.readLine();
            int  countVisitedPerMonth= Integer.parseInt(reader.readLine());
            int ageLimit = Integer.parseInt(reader.readLine());
            double SiteRating = Double.parseDouble(reader.readLine());


            siteList[i] = new Site(id,url,countVisitedPerMonth,ageLimit,SiteRating);
        }

        reader.close();

        return siteList;
    }

    //--------------------File Functions--------------------//

    public static void main(String[] args) throws IOException {
	// write your code here
        Site[] siteList = null;
        Site[] auxiliarySiteList = null;
        Site site = null;

        String fileName,dataFileName,printFileName;

        int menuPoint;

        do {
            Print50Enters();
            PrintAll(siteList);
            PrintMenu();

            menuPoint = GetMenuPoint();

            switch (menuPoint){
                case 1:
                    siteList = AddSite(siteList,site);
                    break;

                case 2:
                    System.out.print("input file name");
                    fileName = scanner.next();
                    dataFileName = fileName+".data";
                    printFileName = fileName+".txt";

                    SaveSiteToFile(siteList,dataFileName);
                    PrintAllToFile(siteList,printFileName);
                    break;

                case 3:
                    System.out.print("Input file name: ");
                    dataFileName = scanner.next()+".data";

                    siteList = LoadSiteFromFile(dataFileName);
                    break;

                case 4:
                    siteList = SortByCountVisitersPerMounth(siteList,true);
                    break;

                case 5:
                    auxiliarySiteList = SortByAge(siteList,InputMinAge(GetMaxAge(siteList),GetMinAge(siteList)),InputMaxAge(GetMaxAge(siteList),GetMinAge(siteList)));
                    Print50Enters();
                    if (auxiliarySiteList == null){
                        System.out.println("not found");
                    }
                    else{
                        PrintAll(auxiliarySiteList);
                    }
                    System.in.read();
                    break;

                case 0:
                    menuPoint = 0;
                    break;

            }

        }while (menuPoint!=0);
    }
}

